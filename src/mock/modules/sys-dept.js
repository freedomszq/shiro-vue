// 生成数据列表
var dataList = [
  {
    'deptId': 1,
    'parentId': 0,
    'parentName': null,
    'name': '技术部',
    'url': null,
    'perms': null,
    'type': 0,
    'icon': 'system',
    'orderNum': 0,
    'open': null,
    'list': null
  },
  {
    'deptId': 2,
    'parentId': 1,
    'parentName': '技术部',
    'name': '技术部1-1',
    'url': 'sys/user',
    'perms': null,
    'type': 1,
    'icon': 'admin',
    'orderNum': 1,
    'open': null,
    'list': null
  }
]
var navDataList = [
  {
    'deptId': 1,
    'parentId': 0,
    'parentName': null,
    'name': '技术部',
    'url': null,
    'perms': null,
    'type': 0,
    'icon': 'system',
    'orderNum': 0,
    'open': null,
    'list': [
      {
        'deptId': 2,
        'parentId': 1,
        'parentName': null,
        'name': '技术部1-1',
        'url': 'sys/user',
        'perms': null,
        'type': 1,
        'icon': 'admin',
        'orderNum': 1,
        'open': null,
        'list': null
      }
    ]
  }
]

// 获取菜单列表
export function list () {
  return {
    // isOpen: false,
    url: '/sys/dept/list',
    type: 'get',
    data: dataList
  }
}

// 获取上级菜单
export function select () {
  let dataList = JSON.parse(JSON.stringify(navDataList))
  dataList = dataList.concat(dataList[0].list)
  return {
    // isOpen: false,
    url: '/sys/dept/select',
    type: 'get',
    data: {
      'msg': 'success',
      'code': 0,
      'deptList': dataList
    }
  }
}

// 获取菜单信息
export function info () {
  return {
    // isOpen: false,
    url: '/sys/dept/info',
    type: 'get',
    data: {
      'msg': 'success',
      'code': 0,
      'dept': dataList[0]
    }
  }
}

// 添加菜单
export function add () {
  return {
    // isOpen: false,
    url: '/sys/dept/save',
    type: 'post',
    data: {
      'msg': 'success',
      'code': 0
    }
  }
}

// 修改菜单
export function update () {
  return {
    // isOpen: false,
    url: '/sys/dept/update',
    type: 'post',
    data: {
      'msg': 'success',
      'code': 0
    }
  }
}

// 删除菜单
export function del () {
  return {
    // isOpen: false,
    url: '/sys/dept/delete',
    type: 'post',
    data: {
      'msg': 'success',
      'code': 0
    }
  }
}
